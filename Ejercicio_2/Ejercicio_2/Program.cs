﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_2
{
    class Program
    {
        static void Main(string[] args)
        {
            String[,] lista = new String[10, 1];
            int i, j;
            String productos;
            String precio;
            int valor;
            int acumulador;
            acumulador = 0;

            for (i = 0; i < 10; i++)
            {
                for (j = 0; j < 1; j++)
                {
                    Console.WriteLine("Ingresa el nombre del producto " + (i + 1));
                    productos = Console.ReadLine();
                    Console.WriteLine("Ingresa el precio del producto " + (i + 1));
                    precio = Console.ReadLine();
                    while (!int.TryParse(precio, out valor))
                    {
                        Console.WriteLine("Debes ingresar un numero, vuelve a intentarlo.");
                        precio = Console.ReadLine();
                    }
                    lista[i, j] = productos + " | precio: " + valor;
                    acumulador = acumulador + valor;
                }
            }

            Console.WriteLine("\nLos productos ingresados y sus precios son:");

            for (i = 0; i < 10; i++)
            {
                for (j = 0; j < 1; j++)
                {
                    Console.WriteLine("[producto " + (i + 1) + " = " + lista[i, j] + "]");
                }
            }
            Console.WriteLine("El total de los productos es: " + acumulador);
            Console.ReadLine();
        }
    }
}
