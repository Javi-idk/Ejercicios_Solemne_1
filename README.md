# Ejercicios de Repaso para Solemne 1

_Ejercicios para desarrollar como repaso para la primera Solemne de Lenguajes de Programación_

### Ejercicios Propuestos 📋

_Detalle de los ejercicios:_

```
1.- Crear un programa que permita ingresar una lista de 10 productos y luego mostrarla en pantalla (utilizar ciclo
for, primer código enseñado).
```
```
2.- Crear una programa que permita ingresar una lista de 10 productos con su respectivo precio (ocupar una variable
por producto, concatenando el precio)

  Ej (pseudocódigo no copiar y pegar, utilizar el código visto en clases) :

   String producto = “Limones”;
   Int precio = 100;
   producto = producto + precio.toString();
```
```
3.- Crear un programa que permita ingresar 10 alumnos, considerar nombre completo y edad, y luego mostrar en pantalla
```
```
4.- Crear un programa que solicite al usuario ingresar un número, luego mostrar un mensaje indicando si el número es
par o impar.
```
```
5.- Crear un programa que solicite un número en pantalla, luego mostrar un mensaje indicando si es unidad, decena 
o centena, el valor ingresado no debe ser mayor a 999.
```

## Autora ✒️

_Javiera Soto_ 

_Primer trimestre de Ingeniería en Computación e Informática. Lenguajes de programación._

⌨️ con ❤️ por [Javi-idk]
