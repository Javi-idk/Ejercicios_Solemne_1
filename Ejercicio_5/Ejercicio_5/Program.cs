﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_5
{
    class Program
    {
        static void Main(string[] args)
        {
            String n;
            int num;

            Console.WriteLine("Ingresa un numero:");
            n = Console.ReadLine();
            int largo = n.Length;
            while (!int.TryParse(n, out num))
            {
                Console.WriteLine("Debes ingresar un numero, vuelve a intentarlo.");
                n = Console.ReadLine();
            }


            if (num >= 1000)
            {
                Console.WriteLine("Debes ingresar un numero menor a 1000");
            }
            else
            {
                if (largo == 1)
                {
                    Console.Write("El numero es una unidad");
                }

                else
                {
                    if (largo == 2)
                    {
                        Console.Write("El numero es una decena");
                    }
                    else
                    {
                        if (largo == 3)
                        {
                            Console.Write("El numero es una centena");
                        }
                    }
                }
            }

            Console.ReadLine();
        }
    }
}
