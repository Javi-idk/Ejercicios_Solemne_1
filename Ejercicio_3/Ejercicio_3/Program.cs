﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_3
{
    class Program
    {
        static void Main(string[] args)
        {
            String[,,] lista = new String[10, 1, 1];
            int i, j, k;
            String productos;
            String apellido;
            String anios;
            int edad;

            Console.WriteLine("El programa te pedirá el nombre y el apellido por separado.");

            for (i = 0; i < 10; i++)
            {
                for (j = 0; j < 1; j++)
                {
                    for (k = 0; k < 1; k++)
                    {
                        Console.WriteLine("\nIngresa el nombre del alumno " + (i + 1));
                        productos = Console.ReadLine();
                        Console.WriteLine("Ingresa el apellido del alumno " + (i + 1));
                        apellido = Console.ReadLine();
                        Console.WriteLine("Ingresa la edad del alumno " + (i + 1));
                        anios = Console.ReadLine();
                        while (!int.TryParse(anios, out edad))
                        {
                            Console.WriteLine("Debes ingresar un numero, vuelve a intentarlo.");
                            anios = Console.ReadLine();
                        }
                        while (edad < 18 || edad > 106)
                        {
                            Console.WriteLine("La edad que ingresaste no es valida, Favor vuelve a intentarlo:");
                            edad = int.Parse(Console.ReadLine());
                        }
                        lista[i, j, k] = productos + " " + apellido + " | edad: " + edad;
                    }
                }
            }

            Console.WriteLine("\nLos alumnos y sus edades son:");

            for (i = 0; i < 10; i++)
            {
                for (j = 0; j < 1; j++)
                {
                    for (k = 0; k < 1; k++)
                    {
                        Console.WriteLine("[Alumno " + (i + 1) + " = " + lista[i, j, k] + "]");
                    }
                }
            }
            Console.ReadLine(); ;
        }
    }
}
