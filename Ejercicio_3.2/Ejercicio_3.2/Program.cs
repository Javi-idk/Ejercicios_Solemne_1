﻿using System;

namespace Ejercicio_3._2
{
    class Program
    {
        static void Main(string[] args)
        {
            int i, j;
            String nombre;
            String acum;
            acum = "";
            String anios;
            int edad;

            for (i = 0; i < 10; i++)
            {
                for (j = 0; j < 1; j++)
                {
                    Console.WriteLine("Ingresa el nombre del alumno: " + (i + 1));
                    nombre = Console.ReadLine();
                    Console.WriteLine("Ingresa la edad del alumno: " + (i + 1));
                    anios = Console.ReadLine();
                    while (!int.TryParse(anios, out edad))
                    {
                        Console.WriteLine("Debes ingresar un numero, vuelve a intentarlo.");
                        anios = Console.ReadLine();
                    }
                    while (edad < 18 || edad > 106)
                    {
                        Console.WriteLine("La edad que ingresaste no es valida, Favor vuelve a intentarlo:");
                        edad = int.Parse(Console.ReadLine());
                    }
                    acum = acum + "[" + nombre + "] " + "[" + edad + "]\n";
                }
            }

            Console.WriteLine("\nLos nombres de los alumnos y sus edades son:");

            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 1; j++)
                {
                    Console.WriteLine(acum);
                }
            }
            Console.ReadLine();
        }
    }
}
