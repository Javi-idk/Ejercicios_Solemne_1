﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_4
{
    class Program
    {
        static void Main(string[] args)
        {
            String n;
            int num;

            Console.WriteLine("Ingresa un numero:");
            n = Console.ReadLine();
            while (!int.TryParse(n, out num))
            {
                Console.WriteLine("Debes ingresar un numero, vuelve a intentarlo.");
                n = Console.ReadLine();
            }

            if ((num % 2) == 0)
            {
                Console.WriteLine("El numero ingresado es un numero par");
            }
            else
            {
                Console.WriteLine("El numero ingresado es un numero impar");
            }
            Console.ReadLine();
        }
    }
}
