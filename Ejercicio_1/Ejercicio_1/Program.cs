﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_1
{
    class Program
    {
        static void Main(string[] args)
        {
            String[] lista = new String[10];
            int i;
            String productos;

            for (i = 0; i < 10; i++)
            {
                Console.WriteLine("Ingresa el nombre del producto " + (i + 1));
                productos = Console.ReadLine();
                lista[i] = productos;
            }

            Console.WriteLine("\nLos productos ingresados son:");

            for (i = 0; i < 10; i++)
            {
                Console.WriteLine("[" + lista[i] + "]");
            }
            Console.ReadLine();
        }
    }
}
